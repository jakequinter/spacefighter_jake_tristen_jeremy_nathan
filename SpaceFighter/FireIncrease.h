#pragma once
#include "GameObject.h"
#include "Blaster.h"
class FireIncrease:public GameObject
{
public:

	FireIncrease();
	virtual ~FireIncrease(){}

	void SetTexture(Texture *pPowerUp) { m_pPowerUp = pPowerUp; }

	virtual void Update(const GameTime *pGameTime);

	virtual void Draw(SpriteBatch *pSpriteBatch);

	virtual std::string ToString() const { return "FireIncrease"; }

	virtual void Initialize(const Vector2 position, const double delaySeconds);

	virtual CollisionType GetCollisionType() const { return CollisionType::POWERUP; }

	virtual float GetSpeed() const { return m_speed; }

	virtual void SetSpeed(const float speed) { m_speed = speed; }

	virtual void Hit(const float damage);

	

protected:
	virtual double GetDelaySeconds() const { return m_delaySeconds; }

	virtual float GetHitPoints() const { return m_hitPoints; }

	virtual float GetMaxHitPoints() const { return m_maxHitPoints; }

	virtual void SetMaxHitPoints(const float hitPoints) { m_maxHitPoints = hitPoints; }
private:

	Texture *m_pPowerUp;

	double m_delaySeconds;

	float m_speed;

	float m_hitPoints;
	float m_maxHitPoints;
	

};