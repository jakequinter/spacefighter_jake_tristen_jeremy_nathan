#include "FireIncrease.h"
#include "Level.h"


FireIncrease::FireIncrease()
{
	SetSpeed(150);
	SetCollisionRadius(20);
	SetMaxHitPoints(1);
	
}



void FireIncrease::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0)
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		if (m_delaySeconds <= 0)
		{
			GameObject::Activate();
		}
		
	}
	

	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		if (!IsOnScreen()) Deactivate();
	}

	GameObject::Update(pGameTime);
}


void FireIncrease::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pPowerUp, GetPosition(), Color::White, m_pPowerUp->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
void FireIncrease::Initialize(const Vector2 position, const double delaySeconds) 
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;
	m_hitPoints = m_maxHitPoints;
	
	
}

void FireIncrease::Hit(const float damage)
{


		m_hitPoints -= damage;

		if (m_hitPoints <= 0)
		{
			GetCurrentLevel()->GetPlayerShip()->IncreaseFireRate();
			GameObject::Deactivate();
		}

}